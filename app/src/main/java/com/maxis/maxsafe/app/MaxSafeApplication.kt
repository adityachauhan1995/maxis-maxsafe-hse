package com.maxis.maxsafe.app

import android.app.Application


/**
 * Created by Sibaprasad Mohanty on 07/06/20.
 * Spm Limited
 * sp.dobest@gmail.com
 */

class MaxSafeApplication : Application() {
    override fun onCreate() {
        super.onCreate()
    }
}