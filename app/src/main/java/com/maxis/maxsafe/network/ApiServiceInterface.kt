package com.maxis.maxsafe.network

import com.android.assignment.network.model.WeatherResponse
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * The interface which provides methods to get result of webservices
 */
interface ApiServiceInterface {

    /**
     * MAXIS api calls
     */

    @GET("/login")
    fun getLogin(@Query("username") userName: String): Observable<List<WeatherResponse>>

    @GET("/validateWorkPermit")
    fun validateWorkPermit(@Query("workPermit") workpermit: String): Observable<List<WeatherResponse>>

    @GET("/validate")
    fun validateSiteAndLogin(@Query("sitename") sitename: String): Observable<List<WeatherResponse>>


    /**
     * Get the list of the pots from the API
     */
    @GET("/posts")
    fun getLogin(): Observable<List<WeatherResponse>>


    /**
     * Get the list of the pots from the API
     */
    @GET("/posts")
    fun getPosts(): Observable<List<WeatherResponse>>

    @GET("forecast")
    fun getWeatherDetails(
        @Query("q") action: String,
        @Query("APPID") appid: String
    ): Call<WeatherResponse>

    companion object Factory {
        fun create(): ApiServiceInterface {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

            val retrofit = retrofit2.Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("BuildConfig.SERVER_URL")
                .build()

            return retrofit.create(ApiServiceInterface::class.java)
        }
    }
}