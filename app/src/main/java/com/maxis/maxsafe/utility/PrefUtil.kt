package com.maxis.maxsafe.utility

import android.content.Context
import androidx.preference.PreferenceManager
import com.maxis.maxsafe.feature.landing.fragment.LandingFragment
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class PrefUtil {
    companion object {


        /**
         * 8 hours in milli seconds
         */
        val TOTAL_WORKING_TIME:Long = 28800

        private const val PREVIOUS_TIMER_LENGTH_SECONDS_ID =
            "com.maxis.maxsafe.previous_timer_length_seconds"
        private const val SECONDS_REMAINING_ID = "com.maxis.maxsafe.seconds_remaining"
        private const val ALARM_SET_TIME_ID = "com.maxis.maxsafe.backgrounded_time"
        private const val WORKING_DAY =
            "com.maxis.maxsafe.workingDay"
        private const val TIMER_STATE_ID = "com.maxis.maxsafe.timer_state"
        private const val TIMER_LENGTH_ID = "com.maxis.maxsafe.timer_length"


        fun getTimerLength(context: Context): Long {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getLong(TIMER_LENGTH_ID, TOTAL_WORKING_TIME)
        }

        fun getPreviousTimerLengthSeconds(context: Context): Long {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getLong(PREVIOUS_TIMER_LENGTH_SECONDS_ID, 0)
        }

        fun setPreviousTimerLengthSeconds(seconds: Long, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putLong(PREVIOUS_TIMER_LENGTH_SECONDS_ID, seconds)
            editor.apply()
        }

        fun setWorkingDay(context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
            val formatted = current.format(formatter)
            editor.putString(WORKING_DAY, formatted)
            editor.apply()
        }

        fun getWorkingDay(context: Context): String? {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getString(WORKING_DAY, "")
        }


        fun getTimerState(context: Context): LandingFragment.TimerState {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val ordinal = preferences.getInt(TIMER_STATE_ID, 0)
            return LandingFragment.TimerState.values()[ordinal]
        }

        fun setTimerState(state: LandingFragment.TimerState, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            val ordinal = state.ordinal
            editor.putInt(TIMER_STATE_ID, ordinal)
            editor.apply()
        }



        fun getSecondsRemaining(context: Context): Long {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getLong(SECONDS_REMAINING_ID, 0)
        }

        fun getMinutesRemaining(context: Context): Long {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return TOTAL_WORKING_TIME
        }

        fun setSecondsRemaining(seconds: Long, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putLong(SECONDS_REMAINING_ID, seconds)
            editor.apply()
        }



        fun getAlarmSetTime(context: Context): Long {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getLong(ALARM_SET_TIME_ID, 0)
        }

        fun setAlarmSetTime(time: Long, context: Context) {
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putLong(ALARM_SET_TIME_ID, time)
            editor.apply()
        }
    }
}