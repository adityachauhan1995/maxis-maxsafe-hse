package com.maxis.maxsafe.utility;

import android.Manifest;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.maxis.maxsafe.R;

import java.io.File;
import java.util.List;
import java.util.Objects;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission_group.CAMERA;


public class ImagePickerActivity extends AppCompatActivity {

    public static final int PICK_FROM_CAMERA = 1;
    public static final int PICK_FROM_GALLERY = 2;

    private static final int PERMISSION_REQUEST_CODE = 101;
    private static final int CROP_IMAGE = 3;
    private int mSize = 300;

    private String[] permissions = {READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};
    private Uri mImageCaptureUri;
    private Uri mSelectedImageUri;
    private BottomSheetDialog bottomSheetDialog;

    private ConstraintLayout imagepickerRoot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_picker);

        imagepickerRoot = findViewById(R.id.imagepickerRoot);

        bottomSheetDialog = new BottomSheetDialog(this, R.style.BottomSheetDialogTheme);

        if (checkPermission()) {
            setupForImagePicker();
        } else {
            requestMultiplePermissions();
        }

    }

    void setupForImagePicker() {
//        showImagePickerBottomSheet();

        if (getIntent().hasExtra("pick_from")) {
            //   mPickFrom = getIntent().getIntExtra("pick_from", PICK_FROM_GALLERY);
        }
        if (getIntent().hasExtra("size")) {
            mSize = getIntent().getIntExtra("size", 300);
        }

        mSelectedImageUri = Uri.fromFile(new File(getExternalCacheDir(), "img" + System.currentTimeMillis() + "tmp_avatar.jpg"));

        showImagePickerBottomSheet();

       /* if (mPickFrom == PICK_FROM_CAMERA)
            startCamera();
        else
            startGallery();*/
    }

    void startCamera() {
        //  mImageCaptureUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + "", createImageFile());
        //	mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "tmp_avatar1.jpg"));
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        startActivityForResult(intent, PICK_FROM_CAMERA);
    }

//    private File createImageFile() {
//        return new File(getExternalCacheDir(), "tmp_avatar1.jpg");
//    }

    void startGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            finish();
            return;
        }
        if (requestCode == PICK_FROM_CAMERA) {
            doCrop();
        } else if (requestCode == PICK_FROM_GALLERY) {
            mImageCaptureUri = data.getData();
            doCrop();
        } else if (requestCode == CROP_IMAGE) {
            if (data == null)
                data = new Intent();
            data.putExtra("data_uri", mSelectedImageUri.getPath());
            setResult(resultCode, data);
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void doCrop() {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, 0);
        int size = list.size();
        if (size == 0) {
            Toast.makeText(this, "Can not find image crop application installed", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            intent.setData(mImageCaptureUri);
            intent.putExtra("outputX", mSize);
            intent.putExtra("outputY", mSize);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);
            // intent.putExtra("return-data", true);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mSelectedImageUri);
            Intent i = new Intent(intent);
            ResolveInfo res = list.get(0);
            i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            startActivityForResult(i, CROP_IMAGE);
        }
    }

    /***************************** END PERMISSION RELATED *****************************/

    void showImagePickerBottomSheet() {
        bottomSheetDialog.setContentView(R.layout.bottomsheet_imagepicker);
        AppCompatButton btnCamera = bottomSheetDialog.findViewById(R.id.btnCamera);
        AppCompatButton btnGallery = bottomSheetDialog.findViewById(R.id.btnGallery);
        assert btnCamera != null;
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCamera();
                bottomSheetDialog.dismiss();
            }
        });
        Objects.requireNonNull(btnGallery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGallery();
                bottomSheetDialog.dismiss();
            }
        });
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });
        bottomSheetDialog.show();
    }

    private void requestMultiplePermissions() {
        // Creating String Array with Permissions.
        ActivityCompat.requestPermissions(this, new String[]
                {
                        CAMERA,
                        WRITE_EXTERNAL_STORAGE,
                }, PERMISSION_REQUEST_CODE);

    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0) {

            boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
            boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

            if (locationAccepted && cameraAccepted) {
                Snackbar.make(imagepickerRoot, "Permission Granted, Now you can access location data and camera.", Snackbar.LENGTH_LONG).show();
                if (checkPermission()) {
                    setupForImagePicker();
                }
            } else {



                if (!locationAccepted) {
                    showMessageOKCancel("You need to allow access to Camera the permissions",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    requestPermissions(new String[]{READ_EXTERNAL_STORAGE, CAMERA},
                                            PERMISSION_REQUEST_CODE);
                                }
                            });
                }

                if (!cameraAccepted) {
                    showMessageOKCancel("You need to allow access to Storage the permissions",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    requestPermissions(new String[]{READ_EXTERNAL_STORAGE},
                                            PERMISSION_REQUEST_CODE);
                                }
                            });
                }

                Snackbar.make(imagepickerRoot, "Permission Denied, You cannot access location data and camera.", Snackbar.LENGTH_LONG).show();

                /*if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
                    showMessageOKCancel("You need to allow access to both the permissions",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    requestPermissions(new String[]{READ_EXTERNAL_STORAGE, CAMERA},
                                            PERMISSION_REQUEST_CODE);
                                }
                            });
                }*/

            }
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(ImagePickerActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
}