package com.maxis.maxsafe.background

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import androidx.preference.PreferenceManager
import com.google.android.gms.location.ActivityRecognitionResult
import com.google.android.gms.location.DetectedActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.maxis.maxsafe.R
import java.lang.reflect.Type;

/**
 * Created by Sibaprasad Mohanty on 11/06/20.
 * Spm Limited
 * sp.dobest@gmail.com
 */

class LocationUpdaterService(sericeName: String) : IntentService(sericeName) {

    override fun onCreate() {
        super.onCreate()
    }

    override fun onHandleIntent(intent: Intent?) {

        if (ActivityRecognitionResult.hasResult(intent)) {

            //If data is available, then extract the ActivityRecognitionResult from the Intent//
            val result: ActivityRecognitionResult = ActivityRecognitionResult.extractResult(intent)

            //Get an array of DetectedActivity objects//
            PreferenceManager.getDefaultSharedPreferences(this)
                .edit()
                .putString(
                    "DETECTED_ACTIVITY",
                    "asd"
                   // detectedActivitiesToJson(result.getProbableActivities())
                )
                .apply()
        }
    }

    fun getActivityString(context: Context, detectedActivityType: Int): String? {
        val resources: Resources = context.resources
        return when (detectedActivityType) {
            DetectedActivity.ON_BICYCLE -> resources.getString(R.string.bicycle)
            DetectedActivity.ON_FOOT -> resources.getString(R.string.foot)
            DetectedActivity.RUNNING -> resources.getString(R.string.running)
            DetectedActivity.STILL -> resources.getString(R.string.still)
            DetectedActivity.TILTING -> resources.getString(R.string.tilting)
            DetectedActivity.WALKING -> resources.getString(R.string.walking)
            DetectedActivity.IN_VEHICLE -> resources.getString(R.string.vehicle)
            else -> resources.getString(R.string.unknown_activity/*, detectedActivityType*/)
        }
    }

    val POSSIBLE_ACTIVITIES = intArrayOf(
        DetectedActivity.STILL,
        DetectedActivity.ON_FOOT,
        DetectedActivity.WALKING,
        DetectedActivity.RUNNING,
        DetectedActivity.IN_VEHICLE,
        DetectedActivity.ON_BICYCLE,
        DetectedActivity.TILTING,
        DetectedActivity.UNKNOWN
    )

    fun detectedActivitiesToJson(detectedActivitiesList: ArrayList<DetectedActivity?>?): String? {
        val type: Type = object : TypeToken<ArrayList<DetectedActivity?>?>() {}.getType()
        return Gson().toJson(detectedActivitiesList, type)
    }

    fun detectedActivitiesFromJson(jsonArray: String?): ArrayList<DetectedActivity?>? {
        val listType: Type = object : TypeToken<ArrayList<DetectedActivity?>?>() {}.getType()
        var detectedActivities: ArrayList<DetectedActivity?>? = Gson().fromJson(jsonArray, listType)
        if (detectedActivities == null) {
            detectedActivities = ArrayList()
        }
        return detectedActivities
    }
}