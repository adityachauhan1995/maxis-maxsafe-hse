package com.maxis.maxsafe.feature.account.staffLogin

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.maxis.maxsafe.BR
import com.maxis.maxsafe.R
import com.maxis.maxsafe.base.BaseFragment
import com.maxis.maxsafe.databinding.FragmentStaffLoginBinding
import com.maxis.maxsafe.feature.account.workpermit.WorkpermitDialogFragment
import com.maxis.maxsafe.feature.common.AppDialogFragment
import kotlinx.android.synthetic.main.fragment_staff_login.*


class StaffLoginFragment : BaseFragment<FragmentStaffLoginBinding, StaffLoginViewmodel>(),
    View.OnClickListener {

    lateinit var bottomSheetDialog: BottomSheetDialog
    private var staffLoginViewmodel: StaffLoginViewmodel? = null

    override fun getBindingVariable(): Int = BR.staffLoginViewmodel

    override fun getLayoutId(): Int = R.layout.fragment_staff_login

    val appDialogFragment: AppDialogFragment by lazy {
        AppDialogFragment.newInstance(
            AppDialogFragment.DIALOG_TYPE_NORMAL,
            "MY Title",
            "messages messages messages messages messages messages messages messages "
        )
    }

    override fun getViewModel(): StaffLoginViewmodel {
        if (staffLoginViewmodel == null) {
            this.staffLoginViewmodel =
                ViewModelProviders.of(this, ViewModelProvider.NewInstanceFactory())
                    .get(StaffLoginViewmodel::class.java)
        }
        return this.staffLoginViewmodel!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
        this.staffLoginViewmodel =
            ViewModelProviders.of(this, ViewModelProvider.NewInstanceFactory())
                .get(StaffLoginViewmodel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvClickheretoEnterWorkpermit.setOnClickListener(this)
    }

    fun showBottomSheetDialog() {
        bottomSheetDialog.setContentView(R.layout.bottomsheet_forgotpassword)
        val tvEmail: AppCompatEditText? = bottomSheetDialog.findViewById(R.id.etEmail)
        val btnSend: AppCompatButton? = bottomSheetDialog.findViewById(R.id.btnSendOtp)

        btnSend?.setOnClickListener {
            bottomSheetDialog.dismiss()
        }
        bottomSheetDialog.show()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvClickheretoEnterWorkpermit -> {
                WorkpermitDialogFragment().show(childFragmentManager, WorkpermitDialogFragment.TAG)
//                AppDialogFragment.newInstance(AppDialogFragment.DIALOG_TYPE_NORMAL, "adasdasdas")
//                    .show(childFragmentManager, "asdasdas")
            }
        }
    }

}