package com.maxis.maxsafe.feature.landing.fragment

import android.app.AlarmManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.android.assignment.utility.CommonUtils
import com.maxis.maxsafe.BR
import com.maxis.maxsafe.R
import com.maxis.maxsafe.base.BaseFragment
import com.maxis.maxsafe.databinding.FragmentLandingBinding
import com.maxis.maxsafe.feature.common.AppDialogFragment
import com.maxis.maxsafe.feature.landing.SosTimerDialogFragment
import com.maxis.maxsafe.utility.GpsUtils
import com.maxis.maxsafe.utility.ImagePickerActivity
import com.maxis.maxsafe.utility.PrefUtil
import kotlinx.android.synthetic.main.fragment_landing.*
import java.util.*


/**
 * Created by Sibaprasad Mohanty on 09/06/20.
 * Spm Limited
 * sp.dobest@gmail.com
 */

class LandingFragment : BaseFragment<FragmentLandingBinding, LandingFragmentViewmodel>(),
    View.OnClickListener {

    lateinit var appDialogFragment: AppDialogFragment

    var isCheckin = false

    val sosTimerDialogFragment: SosTimerDialogFragment by lazy {
        SosTimerDialogFragment()
    }

    private var landingFragmentViewmodel: LandingFragmentViewmodel? = null

    override fun getBindingVariable(): Int = BR.landingfragmentViewModel

    override fun getLayoutId(): Int = R.layout.fragment_landing


    override fun getViewModel(): LandingFragmentViewmodel {
        if (landingFragmentViewmodel == null) {
            this.landingFragmentViewmodel =
                ViewModelProviders.of(this, ViewModelProvider.NewInstanceFactory())
                    .get(LandingFragmentViewmodel::class.java)
        }
        return this.landingFragmentViewmodel!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        GpsUtils(activity).turnGPSOn { isGPSEnable -> // turn on GPS
            if (isGPSEnable) {
                Toast.makeText(activity, "GPS enabled", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(activity, "GPS Disabled", Toast.LENGTH_SHORT).show()
            }
        }

        btncheckInCheckout.setOnClickListener(this)
        btnOnsiteOffSIte.setOnClickListener(this)
        btnExtendWorkingHours.setOnClickListener(this)
        btnHse.setOnClickListener(this)
        btnPtw.setOnClickListener(this)
        imageViewProfile.setOnClickListener(this)

        btnOnsiteOffSIte.isClickable = isCheckin


        appDialogFragment = AppDialogFragment.newInstance(
            AppDialogFragment.DIALOG_TYPE_NORMAL,
            getString(R.string.add_change_profile_pic)
        )

        appDialogFragment.dialogPositiveButtonListener.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer {
                if (it) {
                    activity?.startActivity(Intent(activity, ImagePickerActivity::class.java))
                }
            })

        appDialogFragment.dialogCancelButtonListener.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer {
                if (it) {
                    activity?.startActivity(Intent(activity, ImagePickerActivity::class.java))
                }
            })


        /**
         * Seekbar change lostener
         */
        seekbarSos.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                if (!sosTimerDialogFragment.isVisible) {
                    sosTimerDialogFragment.show(childFragmentManager, "")
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btncheckInCheckout -> {
                if (btncheckInCheckout.text == getString(R.string.checkIn)) {
                    isCheckin = true
                    btncheckInCheckout.text = "Check Out"
                    btnOnsiteOffSIte.isClickable = true
                    btnOnsiteOffSIte.background = activity?.let {
                        ContextCompat.getDrawable(
                            it,
                            R.drawable.button_selector
                        )
                    }
                } else {
                    isCheckin = false
                    btncheckInCheckout.text = "Check In"
                }
                tvOnsiteStatus.text = btncheckInCheckout.text
            }

            R.id.btnOnsiteOffSIte -> {
                if (btnOnsiteOffSIte.text == getString(R.string.onsite)) {

                } else {

                }
                if (isCheckin) {
                    startTimer()
                } else {
                    stopTimer()
                }
                tvOnsiteStatus.text = btnOnsiteOffSIte.text
            }

            R.id.btnExtendWorkingHours -> {
                AppDialogFragment.newInstance(
                    AppDialogFragment.DIALOG_TYPE_SPINNER,
                    "Extend Work hours"
                ).show(childFragmentManager, AppDialogFragment.TAG)
            }

            R.id.btnHse -> {

            }

            R.id.btnPtw -> {

            }
            R.id.imageViewProfile -> {
                appDialogFragment.show(childFragmentManager, "AppDialogFragment")
            }
        }
    }

    companion object {
        val TAG = LandingFragment::class.java
        fun setAlarm(context: Context, nowSeconds: Long, secondsRemaining: Long): Long {
            val wakeUpTime = (nowSeconds + secondsRemaining) * 1000
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
//            val intent = Intent(context, TimerExpiredReceiver::class.java)
//            val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0)
//            alarmManager.setExact(AlarmManager.RTC_WAKEUP, wakeUpTime, pendingIntent)
//            PrefUtil.setAlarmSetTime(nowSeconds, context)
            return wakeUpTime
        }

        fun removeAlarm(context: Context) {
//            val intent = Intent(context, TimerExpiredReceiver::class.java)
//            val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0)
//            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
//            alarmManager.cancel(pendingIntent)
//            PrefUtil.setAlarmSetTime(0, context)
        }

        val nowSeconds: Long
            get() = Calendar.getInstance().timeInMillis / 1000
    }

    enum class TimerState {
        Stopped, Paused, Running
    }

    private lateinit var timer: CountDownTimer
    private var timerLengthSeconds: Long = PrefUtil.TOTAL_WORKING_TIME
    private var timerState = TimerState.Stopped

    private var secondsRemaining: Long = PrefUtil.TOTAL_WORKING_TIME


    override fun onResume() {
        super.onResume()

        initTimer()

//        removeAlarm(this)
//        NotificationUtil.hideTimerNotification(this)
    }

    override fun onPause() {
        super.onPause()
        stopTimer()
    }

    private fun initTimer() {
        timerState = PrefUtil.getTimerState(activity as Context)
        //we don't want to change the length of the timer which is already running
        //if the length was changed in settings while it was backgrounded
        if (timerState == TimerState.Stopped)
            setNewTimerLength()
        else
            setPreviousTimerLength()

        secondsRemaining = if (timerState == TimerState.Running || timerState == TimerState.Paused)
            PrefUtil.getMinutesRemaining(activity as Context)
        else
            timerLengthSeconds

        /* val alarmSetTime = PrefUtil.getAlarmSetTime(activity as Context)

         if (alarmSetTime > 0)
             secondsRemaining -= nowSeconds - alarmSetTime*/

        if (secondsRemaining <= 0)
            onTimerFinished()
        else if (timerState == TimerState.Running)
            startTimer()

        updateCountdownUI()
    }

    private fun onTimerFinished() {
        timerState = TimerState.Stopped
        setNewTimerLength()
        secondsRemaining = timerLengthSeconds
        updateCountdownUI()
    }

    private fun startTimer() {
        timerState = TimerState.Running
        timer = object : CountDownTimer(secondsRemaining * 1000, 1000) {
            override fun onFinish() = onTimerFinished()
            override fun onTick(millisUntilFinished: Long) {
                secondsRemaining = millisUntilFinished / 1000
                updateCountdownUI()
            }
        }.start()
    }

    private fun stopTimer() {
        if (timerState == TimerState.Running) {
            timer.cancel()
//            val wakeUpTime = setAlarm(this, nowSeconds, secondsRemaining)
//            NotificationUtil.showTimerRunning(this, wakeUpTime)
        } else if (timerState == TimerState.Paused) {
//            NotificationUtil.showTimerPaused(this)
        }

//        PrefUtil.setPreviousTimerLengthSeconds(timerLengthSeconds, activity as Context)
//        PrefUtil.setSecondsRemaining(secondsRemaining, activity as Context)
//        PrefUtil.setTimerState(timerState, activity as Context)
    }

    private fun setNewTimerLength() {
        val lengthInMinutes = PrefUtil.getTimerLength(activity as Context)
//        timerLengthSeconds = (lengthInMinutes * 60L)
//        progressBar.max = timerLengthSeconds.toInt()
        progressBar.max = lengthInMinutes.toInt()
    }

    private fun setPreviousTimerLength() {

    }

    private fun updateCountdownUI() {
        val minutesUntilFinished = secondsRemaining / 60
        val secondsInMinuteUntilFinished = secondsRemaining - minutesUntilFinished * 60
        val secondsStr = secondsInMinuteUntilFinished.toString()
        tvCounter.text =
            CommonUtils.getHourMinuteSecond(PrefUtil.TOTAL_WORKING_TIME - secondsRemaining)
//       "$minutesUntilFinished:${if (secondsStr.length == 2) secondsStr else "0" + secondsStr}"
        progressBar.progress = (timerLengthSeconds - secondsRemaining).toInt()
    }
}