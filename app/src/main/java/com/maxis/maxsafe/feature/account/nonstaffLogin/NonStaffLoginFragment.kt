package com.maxis.maxsafe.feature.account.nonstaffLogin

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.maxis.maxsafe.BR
import com.maxis.maxsafe.R
import com.maxis.maxsafe.base.BaseDialogFragment
import com.maxis.maxsafe.databinding.FragmentNonstaffLoginBinding

class NonStaffLoginFragment :
    BaseDialogFragment<FragmentNonstaffLoginBinding, NonStaffLoginViewmodel>() {

    private lateinit var nonStaffLoginViewmodel: NonStaffLoginViewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
        this.nonStaffLoginViewmodel =
            ViewModelProviders.of(this, ViewModelProvider.NewInstanceFactory())
                .get(NonStaffLoginViewmodel::class.java)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle(getString(R.string.nonstaff_login))
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            NonStaffLoginFragment().apply {

            }
    }

    override fun getBindingVariable(): Int = BR.nonstaffLoginViewmodel

    override fun getLayoutId(): Int = R.layout.fragment_nonstaff_login

    override fun getViewModel(): NonStaffLoginViewmodel = nonStaffLoginViewmodel
}