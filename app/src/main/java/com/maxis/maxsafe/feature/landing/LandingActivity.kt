package com.maxis.maxsafe.feature.landing

import androidx.lifecycle.ViewModelProvider
import com.maxis.maxsafe.BR
import com.maxis.maxsafe.R
import com.maxis.maxsafe.base.BaseActivity
import com.maxis.maxsafe.databinding.ActivityLandingBinding
import com.maxis.maxsafe.feature.landing.fragment.LandingFragment

class LandingActivity : BaseActivity<ActivityLandingBinding, LandingViewModel>() {

    lateinit var landingViewModel: LandingViewModel

    override fun getBindingVariable(): Int = BR.landingViewmodel

    override fun getLayoutId(): Int = R.layout.activity_landing

    override fun getViewModel(): LandingViewModel {
        landingViewModel = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(LandingViewModel::class.java)
        return landingViewModel
    }

    override fun onResume() {
        super.onResume()
        supportFragmentManager.beginTransaction().add(R.id.fragmentContainer, LandingFragment())
            .commit()
    }
}