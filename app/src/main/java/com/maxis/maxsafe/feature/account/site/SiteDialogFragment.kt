package com.maxis.maxsafe.feature.account.site

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.maxis.maxsafe.BR
import com.maxis.maxsafe.R
import com.maxis.maxsafe.base.BaseDialogFragment
import com.maxis.maxsafe.databinding.DialogfragmentSiteBinding
import com.maxis.maxsafe.feature.landing.LandingActivity
import kotlinx.android.synthetic.main.dialogfragment_site.*


class SiteDialogFragment : BaseDialogFragment<DialogfragmentSiteBinding, SiteViewModel>(),
    View.OnClickListener {

    var selectedSite = ""

    private var siteViewModel: SiteViewModel? = null

    override fun getBindingVariable(): Int = BR.workpermitViewmodel

    override fun getLayoutId(): Int = R.layout.dialogfragment_site

    override fun getViewModel(): SiteViewModel {
        if (siteViewModel == null) {
            this.siteViewModel =
                ViewModelProviders.of(this, ViewModelProvider.NewInstanceFactory())
                    .get(SiteViewModel::class.java)
        }
        return this.siteViewModel!!
    }

    companion object {
        val TAG = SiteDialogFragment::class.java.simpleName
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showHideToolbar(false)
        tvBacktoStaffloginpage.setOnClickListener(this)
        btnSubmit.setOnClickListener(this)
        loadSpinnerData()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvBacktoStaffloginpage -> {
                dismissAllowingStateLoss()
            }
            R.id.btnSubmit -> {

                if (!TextUtils.isEmpty(selectedSite)) {

                    btnSubmit.visibility = View.GONE
                    progressBarSite.visibility = View.VISIBLE
                    Handler().postDelayed(kotlinx.coroutines.Runnable {
                        activity?.startActivity(Intent(activity, LandingActivity::class.java))
                    }, 3000)
                } else {
                    Toast.makeText(activity, "Select site", Toast.LENGTH_SHORT).show()
                }

            }
        }
    }

    fun loadSpinnerData() {

        val siteArray = activity?.resources?.getStringArray(R.array.site_array)

        activity?.let {
            ArrayAdapter.createFromResource(
                it,
                R.array.site_array,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                // Apply the adapter to the spinner
                spinnerSite.adapter = adapter
            }
        }
        spinnerSite.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedSite = ""
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                selectedSite = siteArray?.get(position) ?: ""
            }
        }
    }
}