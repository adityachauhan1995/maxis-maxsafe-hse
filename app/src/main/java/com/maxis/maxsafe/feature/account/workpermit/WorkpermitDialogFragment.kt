package com.maxis.maxsafe.feature.account.workpermit

import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.maxis.maxsafe.BR
import com.maxis.maxsafe.R
import com.maxis.maxsafe.base.BaseDialogFragment
import com.maxis.maxsafe.databinding.DialogfragmentWorkpermitBinding
import com.maxis.maxsafe.feature.account.site.SiteDialogFragment
import kotlinx.android.synthetic.main.dialogfragment_workpermit.*

class WorkpermitDialogFragment :
    BaseDialogFragment<DialogfragmentWorkpermitBinding, WorkpermitViewmodel>(),
    View.OnClickListener {

    private var workpermitViewmodel: WorkpermitViewmodel? = null

    override fun getBindingVariable(): Int = BR.siteViewmodel

    override fun getLayoutId(): Int = R.layout.dialogfragment_workpermit

    override fun getViewModel(): WorkpermitViewmodel {
        if (workpermitViewmodel == null) {
            this.workpermitViewmodel =
                ViewModelProviders.of(this, ViewModelProvider.NewInstanceFactory())
                    .get(WorkpermitViewmodel::class.java)
        }
        return this.workpermitViewmodel!!
    }

    companion object {
        val TAG = WorkpermitDialogFragment::class.java.simpleName
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showHideToolbar(false)
        btnNext.setOnClickListener(this)
        tvBacktoStaffLoginpage.setOnClickListener(this)
        radioWorkpermit.isChecked = true
        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == R.id.radioWorkpermit) {
                etWorkPermitOrDocket.hint = getString(R.string.enter_your_workpermit)
                tvWorkpermit.text = getString(R.string.workpermit)
            } else {
                etWorkPermitOrDocket.hint = getString(R.string.enter_your_docket)
                tvWorkpermit.text = getString(R.string.docket)
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvBacktoStaffLoginpage -> {
                dismissAllowingStateLoss()
            }
            R.id.btnNext -> {
                val workpermitOrDOcket = etWorkPermitOrDocket.text.toString()
                if (TextUtils.isEmpty(workpermitOrDOcket)) {
                    Toast.makeText(activity, "Enter Workpermit Or Docket", Toast.LENGTH_SHORT)
                        .show()
                    return
                }
                btnNext.visibility = View.GONE
                progressBarWorkPermit.visibility = View.VISIBLE
                Handler().postDelayed(kotlinx.coroutines.Runnable {
                    SiteDialogFragment().show(childFragmentManager, SiteDialogFragment.TAG)
                }, 3000)

            }
        }
    }
}