package com.maxis.maxsafe.feature.account.site

import androidx.lifecycle.MutableLiveData
import spm.androidworld.all.mvvmWithDataBinding.base.BaseRepository


/**
 * Created by Sibaprasad Mohanty on 11/06/20.
 * Spm Limited
 * sp.dobest@gmail.com
 */

class SiteRepository : BaseRepository(){

    override fun showProgress(): MutableLiveData<Boolean> = MutableLiveData(true)

    override fun showError(): MutableLiveData<String> = MutableLiveData("Error")

}