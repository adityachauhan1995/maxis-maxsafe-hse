package com.maxis.maxsafe.feature.account.staffLogin

import android.content.Intent
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.*
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import com.android.assignment.utility.CommonUtils
import com.maxis.maxsafe.base.BaseViewModel
import com.maxis.maxsafe.feature.landing.LandingActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


/**
 * Created by Sibaprasad Mohanty on 03/06/20.
 * Spm Limited
 * sp.dobest@gmail.com
 */

class StaffLoginViewmodel : BaseViewModel(), LifecycleObserver,
    Observable {

    private val registry = PropertyChangeRegistry()

    val staffLoginRepository: StaffLoginRepository by lazy {
        StaffLoginRepository()
    }

    @Bindable
    var progressVisibility: ObservableBoolean = ObservableBoolean(false)

    val forgotPwdEmail = MutableLiveData<String>()

    @Bindable
    var emailStr = ObservableField<String>()

    @Bindable
    var passwordStr = ObservableField<String>()

    @Bindable
    var emailError = ObservableField<String>("")

    @Bindable
    var passwordError = ObservableField<String>("")

    var email: String = ""
    var password: String = ""

    fun setEmail_Id(email: String) {
        this.email = email
        emailStr.set(email)
    }

    fun getEmail_Id(): String {
        return email
    }

    fun setPwd(pwd: String) {
        this.password = pwd
        passwordStr.set(pwd)
    }

    fun getPwd(): String {
        return password
    }

    fun onLoginClicked(view: View) {
        progressVisibility.set(true)

        if (!CommonUtils.isValidEmail(email)) {
            emailError.set("Invalid User Name")
            progressVisibility.set(false)
            Toast.makeText(view.context, emailError.get(), Toast.LENGTH_SHORT).show()
            return
        } else if (password.length < 6) {
            progressVisibility.set(false)
            passwordError.set("Password can not be less than 6")
            Toast.makeText(view.context, passwordError.get(), Toast.LENGTH_SHORT).show()
            return
        } else {
            progressVisibility.set(true)
            Handler().postDelayed({
                view.context.startActivity(Intent(view.context, LandingActivity::class.java))
                progressVisibility.set(false)
            }, 5000)

            /**
             * MAKE API CALL
             */
            GlobalScope.launch(Dispatchers.IO) {
                val dummyResponse = staffLoginRepository.makeLoginRequest("")
            }
        }
    }

    fun onTextChangedEmail(
        s: CharSequence,
        start: Int,
        before: Int,
        count: Int
    ) {
        Log.w("tag", "onTextChanged $s")
    }

    fun onTextChangedPassword(
        s: CharSequence,
        start: Int,
        before: Int,
        count: Int
    ) {
        Log.w("tag", "onTextChanged $s")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun getData() {
//        progressVisibility = true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun start() {
        Log.e("TAG", "================================>>>> START lifecycle owner STARTED")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stop() {
        Log.e("TAG", "================================>>>> STOP lifecycle owner STOPED")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun resume() {
        Log.e("TAG", "================================>>>> RESUME lifecycle owner STARTED")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun pause() {
        Log.e("TAG", "================================>>>> PAUSE lifecycle owner STARTED")
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        registry.remove(callback)
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        registry.add(callback)
    }
}