package com.maxis.maxsafe.feature.landing

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import com.maxis.maxsafe.R
import kotlinx.android.synthetic.main.dialogfragment_sos_timer.*


/**
 * Created by Sibaprasad Mohanty on 13/06/20.
 * Spm Limited
 * sp.dobest@gmail.com
 */

class SosTimerDialogFragment : DialogFragment(), View.OnClickListener {

    val totalTime: Long = 20000
    var currentProgres = 0

    override fun onStart() {
        super.onStart()
        dialog?.let {
            it.window?.setWindowAnimations(
                R.style.styleDialogFragment
            )
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            it.window?.setLayout(width, height)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // the content
        val root = RelativeLayout(activity)
        root.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )

        // creating the fullscreen dialog
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        dialog?.let {
            it.window?.let { window ->
                window
                    .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                window.setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            }
        }
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialogfragment_sos_timer, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        timer.start()
        btnOkay.setOnClickListener(this)
        btnCancel.setOnClickListener(this)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
    }

    override fun onPause() {
        super.onPause()
        timer.cancel()
    }

    val timer = object : CountDownTimer(totalTime, 1000) {
        override fun onTick(millisUntilFinished: Long) {
            currentProgres = currentProgres + 1
            tvCounter.text = "" + (20 - currentProgres)
            progressBarTimer.progress = currentProgres
        }

        override fun onFinish() {

        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnCancel -> {
                finishTimer()
                dismissAllowingStateLoss()
            }
            R.id.btnOkay -> {
                finishTimer()
                dismissAllowingStateLoss()
            }
        }
    }

    fun finishTimer() {
        currentProgres = 0
        timer.cancel()
    }
}