package com.maxis.maxsafe.feature.common

import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.MutableLiveData
import com.maxis.maxsafe.R
import kotlinx.android.synthetic.main.fragment_commondialog.*


/**
 * Created by Sibaprasad Mohanty on 11/06/20.
 * Spm Limited
 * sp.dobest@gmail.com
 */

class AppDialogFragment : DialogFragment() {

    var dialogType = 0
    var dialogTitle: String? = ""
    var dialogMessage: String? = ""

    public val dialogCancelButtonListener: MutableLiveData<Boolean> = MutableLiveData(false)
    public val dialogPositiveButtonListener: MutableLiveData<Boolean> = MutableLiveData(false)

    companion object {

        val TAG = "AppDialogFragment"
        val DIALOG_TYPE_NORMAL = 1
        val DIALOG_TYPE_SPINNER = 2
        val DIALOG_TYPE_FORM = 3
        val key_dialogType = "DIALOGTYPE"
        val key_dialogTitle = "DIALOG_TITLE"
        val key_dialogMessage = "DIALOG_MESSAGE"

        fun newInstance(dialogType: Int, title: String, message: String = ""): AppDialogFragment {
            val args = Bundle()
            args.putInt(key_dialogType, dialogType)
            args.putString(key_dialogTitle, title)
            args.putString(key_dialogMessage, message)
            val fragment = AppDialogFragment()
            fragment.arguments = args
            return fragment
        }

    }

    override fun onStart() {
        super.onStart()
        dialog?.let {
            it.setCanceledOnTouchOutside(true)
            it.window?.setWindowAnimations(
                R.style.styleDialog
            )

//            val back = ColorDrawable(Color.TRANSPARENT)
//            val inset = InsetDrawable(back, 40)
//            dialog!!.window!!.setBackgroundDrawable(inset)

            val window: Window? = dialog!!.window
            val size = Point()

            val display: Display? = window?.windowManager?.defaultDisplay
            display?.getSize(size)

            val width1: Int = size.x
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
            window?.setLayout((width1 * 0.8).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)

//            val width = ViewGroup.LayoutParams.MATCH_PARENT
//            val height = ViewGroup.LayoutParams.WRAP_CONTENT
//            it.window?.setLayout(width, height)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
            STYLE_NORMAL,
            R.style.styleDialog
        )

        arguments?.let {
            if (it.containsKey(key_dialogType)) {
                dialogType = it.getInt(key_dialogType)
            }
            if (it.containsKey(key_dialogTitle)) {
                dialogTitle = it.getString(key_dialogTitle)
            }
            if (it.containsKey(key_dialogMessage)) {
                dialogMessage = it.getString(key_dialogMessage)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var view: View? = null

        when (dialogType) {
            DIALOG_TYPE_NORMAL, DIALOG_TYPE_SPINNER -> {
                view = inflater.inflate(R.layout.fragment_commondialog, container, false)
            }
            DIALOG_TYPE_FORM -> {
                view = inflater.inflate(R.layout.dialog_site_details, container, false)
            }
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when (dialogType) {
            DIALOG_TYPE_NORMAL, DIALOG_TYPE_SPINNER -> {
                if (TextUtils.isEmpty(dialogMessage)) {
                    tvDialogMessage.visibility = View.GONE
                } else {
                    tvDialogMessage.text = dialogMessage
                }
                if (!TextUtils.isEmpty(dialogTitle)) {
                    tvDialogTitle.text = dialogTitle
                }
                buttonPositive.setOnClickListener {
                    dismissAllowingStateLoss()
                    dialogPositiveButtonListener.value = true
                }
                buttonCancel.setOnClickListener {
                    dismissAllowingStateLoss()
                    dialogCancelButtonListener.value = true
                }
                if (dialogType == DIALOG_TYPE_SPINNER) {
                    spinnerCount.visibility = View.VISIBLE
                    tvDialogMessage.visibility = View.GONE
                    loadSpinnerData()
                }
            }
        }
    }

    fun setDialogTitleAndMessage(title: String, message: String) {
        this.dialogTitle = title
        this.dialogMessage = message
        if (dialogType == DIALOG_TYPE_NORMAL) {
            if (TextUtils.isEmpty(dialogMessage)) {
                tvDialogMessage.visibility = View.GONE
            }
            if (!TextUtils.isEmpty(dialogTitle)) {
                tvDialogTitle.text = dialogMessage
            }
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    fun loadSpinnerData() {
        activity?.let {
            ArrayAdapter.createFromResource(
                it,
                R.array.countArray,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                // Apply the adapter to the spinner
                spinnerCount.adapter = adapter
            }
        }
    }
}