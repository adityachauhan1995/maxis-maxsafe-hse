package com.maxis.maxsafe.feature.account.forgotPassword

import com.maxis.maxsafe.R
import com.maxis.maxsafe.base.BaseBottomSheetDialogFragment

/**
 * Created by Sibaprasad Mohanty on 07/06/20.
 * Spm Limited
 * sp.dobest@gmail.com
 */

class MyBottomSheetDialogFragment : BaseBottomSheetDialogFragment() {
    override fun getLayoutId(): Int = R.layout.bottomsheet_forgotpassword
}