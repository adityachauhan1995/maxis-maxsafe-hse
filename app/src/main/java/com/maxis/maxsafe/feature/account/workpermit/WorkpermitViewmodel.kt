package com.maxis.maxsafe.feature.account.workpermit

import android.content.Intent
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.android.assignment.utility.CommonUtils
import com.maxis.maxsafe.base.BaseViewModel
import com.maxis.maxsafe.feature.landing.LandingActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * Created by Sibaprasad Mohanty on 08/06/20.
 * Spm Limited
 * sp.dobest@gmail.com
 */

class WorkpermitViewmodel : BaseViewModel(), LifecycleObserver,
    Observable {

    val workPermitRepository: WorkPermitRepository by lazy {
        WorkPermitRepository()
    }

    init {

    }

    var progressVisibility: ObservableBoolean = ObservableBoolean()

    @Bindable
    var workpermitStr = ObservableField<String>()

    var workpermit: String = ""

    fun setWork_permit(workpermit: String) {
        this.workpermit = workpermit
        workpermitStr.set(workpermit)
    }

    fun getEmail_Id(): String {
        return workpermit
    }

    fun onSubmitClicked(view: View) {

        progressVisibility.set(true)

        if (!CommonUtils.isValidEmail(workpermit)) {
            progressVisibility.set(false)
            Toast.makeText(view.context, "Invalid Workpermit", Toast.LENGTH_SHORT).show()
            return
        }

        Handler().postDelayed({
            view.context.startActivity(Intent(view.context, LandingActivity::class.java))
        }, 3000)

        GlobalScope.launch(Dispatchers.IO) {
            val dummyResponse = workPermitRepository.checkWorkpermit("")
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun getData() {
        // progressVisibility = true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun start() {
        Log.e("TAG", "================================>>>> START lifecycle owner STARTED")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stop() {
        Log.e("TAG", "================================>>>> STOP lifecycle owner STOPED")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun resume() {
        Log.e("TAG", "================================>>>> RESUME lifecycle owner STARTED")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun pause() {
        Log.e("TAG", "================================>>>> PAUSE lifecycle owner STARTED")
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }

}