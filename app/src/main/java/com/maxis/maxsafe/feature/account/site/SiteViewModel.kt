package com.maxis.maxsafe.feature.account.site

import android.content.Intent
import android.os.Handler
import android.view.View
import androidx.databinding.Observable
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LifecycleObserver
import com.maxis.maxsafe.base.BaseViewModel
import com.maxis.maxsafe.feature.landing.LandingActivity

/**
 * Created by Sibaprasad Mohanty on 08/06/20.
 * Spm Limited
 * sp.dobest@gmail.com
 */

class SiteViewModel : BaseViewModel(), LifecycleObserver,
    Observable {

    var progressVisibility: ObservableBoolean = ObservableBoolean()


    fun onSubmitClicked(view: View) {
        progressVisibility.set(true)

        Handler().postDelayed({
            view.context.startActivity(Intent(view.context, LandingActivity::class.java))
        }, 3000)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }

}