package com.maxis.maxsafe.feature.account

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.maxis.maxsafe.BR
import com.maxis.maxsafe.R
import com.maxis.maxsafe.base.BaseActivity
import com.maxis.maxsafe.base.BaseViewModel
import com.maxis.maxsafe.databinding.ActivityOnboardingBinding
import com.maxis.maxsafe.feature.account.staffLogin.StaffLoginFragment


/**
 * Created by Sibaprasad Mohanty on 07/06/20.
 * Spm Limited
 * sp.dobest@gmail.com
 */

class OnboardingActivity : BaseActivity<ActivityOnboardingBinding, BaseViewModel>() {

    lateinit var onboardingViewModel: OnboardingViewModel

    override fun getBindingVariable(): Int = BR.onBoardingViewmodel

    override fun getLayoutId(): Int = R.layout.activity_onboarding

    override fun getViewModel(): BaseViewModel {
        onboardingViewModel = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(OnboardingViewModel::class.java)
        return onboardingViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableLeftNavigationDrawer(false)
        hideToolbar()
        supportFragmentManager.beginTransaction()
            .setCustomAnimations(
                R.anim.slide_in_from_right, R.anim.slide_out_to_left,
                R.anim.slide_in_from_left, R.anim.slide_out_to_right
            )
            .replace(R.id.fragmentContainerView, StaffLoginFragment())
            .commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finish()
        } else {
            super.onBackPressed()
        }
    }
}