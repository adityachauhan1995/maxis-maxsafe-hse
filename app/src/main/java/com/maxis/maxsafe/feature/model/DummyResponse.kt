package com.maxis.maxsafe.feature.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Sibaprasad Mohanty on 11/06/20.
 * Spm Limited
 * sp.dobest@gmail.com
 */

@Parcelize
data class DummyResponse(val responseMessage:String,val responseCode:Int): Parcelable