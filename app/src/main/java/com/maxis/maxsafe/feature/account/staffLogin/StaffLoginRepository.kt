package com.maxis.maxsafe.feature.account.staffLogin

import androidx.lifecycle.MutableLiveData
import com.maxis.maxsafe.feature.model.DummyResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import spm.androidworld.all.mvvmWithDataBinding.base.BaseRepository


/**
 * Created by Sibaprasad Mohanty on 11/06/20.
 * Spm Limited
 * sp.dobest@gmail.com
 */

class StaffLoginRepository : BaseRepository(){

    override fun showProgress(): MutableLiveData<Boolean> = MutableLiveData(true)

    override fun showError(): MutableLiveData<String> = MutableLiveData("Error")

    suspend fun makeLoginRequest(
        jsonBody: String
    ): DummyResponse {
        // Move the execution of the coroutine to the I/O dispatcher
        return withContext(Dispatchers.IO) {
            DummyResponse("sds", 200)
        }
    }

}