package com.maxis.maxsafe.base

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.LayoutRes
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.maxis.maxsafe.R


/**
 * Created by Sibaprasad Mohanty on 07/06/20.
 * Spm Limited
 * sp.dobest@gmail.com
 */

abstract class BaseBottomSheetDialogFragment : BottomSheetDialogFragment() {

    @LayoutRes
    abstract fun getLayoutId(): Int

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        BottomSheetDialog(requireContext(), theme)

    @Nullable
    override fun onCreateView(
        @NonNull inflater: LayoutInflater, @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        val rootView: View = inflater.inflate(R.layout.fragment_base, container, false)
        val myVIew = inflater.inflate(getLayoutId(), container, false)
        val frameLayout = rootView.findViewById<FrameLayout>(R.id.fragment_layout_container)
        frameLayout.addView(myVIew)
        return rootView
    }
}