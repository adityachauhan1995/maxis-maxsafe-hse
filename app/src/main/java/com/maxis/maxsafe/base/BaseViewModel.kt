package com.maxis.maxsafe.base

import androidx.lifecycle.ViewModel


/**
 * Created by Sibaprasad Mohanty on 31/05/20.
 * Spm Limited
 * sp.dobest@gmail.com
 */

open class BaseViewModel : ViewModel() {

}