package com.maxis.maxsafe.base

import com.google.android.material.bottomsheet.BottomSheetDialogFragment

/**
 * Created by Sibaprasad Mohanty on 06/06/20.
 * Spm Limited
 * sp.dobest@gmail.com
 */

abstract class BaseBottomsheet : BottomSheetDialogFragment() {

}