# Maxi-Safe Application
  
## Technologies used
- **Language -** Kotlin 3.6
- **IDE -** Android Studio 4.0
- **Architecture Design Pattern -** MVVM
- **Funcional Design Pattern -** Singleton, Observable
  
## Android Concepts Used
- **Koin** Dependency Injection Library
- Material Design
- RxJava3
- Android Jet pack
- JUnit for test cases
- Espresso for UI test cases
- Clean Architecture
- Solid Principle
- 

